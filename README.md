# Minimax - Jogo da velha

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2016<br />

### Objetivo
Implementação do jogo da velha (tic tac toe) aplicando o minimax como inteligência artificial do oponente.

### Observação

IDE:  [IntelliJ IDEA](https://www.jetbrains.com/idea/)<br />
Linguagem: [JAVA](https://www.java.com/)<br />
Banco de dados: Não utiliza<br />

### Execução

    $ cd src/
    $ javac *.java
    $ java JogadaAtual
    
### Contribuição

Esse projeto está concluído e livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->