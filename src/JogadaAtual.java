/**
 * Objeto que salva a jogada seja da IA ou do usuario
 * Salva um objeto Jogada e a pontuação dela 
 * @author Paulo Leal
 *
 */
public class JogadaAtual {
    int pontuacao;
    Jogada jogada;
    
    // Construtor padrão
    JogadaAtual(int pontuacao, Jogada jogada) {
        this.pontuacao = pontuacao;
        this.jogada = jogada;
    }
}
