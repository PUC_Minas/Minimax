/**
 * Classe principal do Jogo da Velha
 * @author Paulo Leal
 *
 */
public class JogoDaVelha {

	/**
	 * Chamada do metodo principal
	 * @param args
	 */
    public static void main(String[] args) {
    	// Cria uma nova estrutura para o jogo
        Estrutura estrutura = new Estrutura();
        // Mostra na tela a estrutura criada
        estrutura.mostrar();

        // O jogo so acaba se nao existir jogadas validas
        while (!estrutura.existeJogadaValida()) {
            int x = -1;
            int y = -1;
            // Recebe os valores do usuario
            System.out.println("Sua vez: ");
            while(x < 0 || x > 2 ) {
                System.out.print("Escolha uma linha [0][1][2]: ");
                try{
                	x = estrutura.entrada.nextInt();
                }catch(Exception e){
                	System.out.println("Valor invalido para linha! ");
                	estrutura.entrada.nextLine();
                }
            }
            while(y < 0 || y >2) {
                System.out.print("Agora escolha uma coluna [0][1][2]: ");
                try{
                	y = estrutura.entrada.nextInt();
                }catch(Exception e){
                	System.out.println("Valor invalido para coluna! ");
                	estrutura.entrada.nextLine();
                }
            }
            // Cria uma nova jogada com os dados usuario
            Jogada vezDoUsuario = new Jogada(x,y);
            // Insere a jogada na estrura
            estrutura.fazerJogada(vezDoUsuario, 2); //2 for O and O is the user
            // Mostra o resultado
            estrutura.mostrar();
            // CondiÃ§Ã£o de parada
            if (estrutura.existeJogadaValida()) {
                break;
            }
            // Jogada da IA - utiliza o minimax para gerar a arvore e propagar a pontuacao
            System.out.println("Vez do computador, aguarde: ");
            
            // Normal descomente essas linhas
            estrutura.chamadaMinimax(0, 1);
            estrutura.fazerJogada(estrutura.encontrarMelhorMovimento(), 1);

            // Alpha Beta descomente essas linhas
            //estrutura.minimaxAlphaBeta(0,1);
            //estrutura.fazerJogada(estrutura.jogadaDaIA, 1);
            
            // Mostra a jogada da IA
            estrutura.mostrar();
        }
        // Mensagens de fim de jogo
        System.out.println("-- FIM DE JOGO --");
        if (estrutura.vitoriaDaIA()) {
            System.out.println("Voce perdeu! Tente novamente!");
        } else if (estrutura.vitoriaDoUsuario()) {
            System.out.println("Parabens, voce ganhou!.");
        } else {
            System.out.println("Empatou!");
        }
    }
    
}
