/**
 * Objeto de jogada, guarda as posições da matriz setadas
 * @author Paulo Leal
 *
 */
public class Jogada {
    int x, y;

    // Construtor padrão
    public Jogada(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
