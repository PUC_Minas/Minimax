// Dependencias
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Classe com toda a estrutura do jogo
 * @author Paulo Leal
 *
 */
public class Estrutura {
    List<Jogada> possiveisJogadas;
    List<JogadaAtual> pontuacaoDasFolhas;
    Scanner entrada = new Scanner(System.in);
    int[][] estrutura = new int[3][3];
    
    // Para a poda AlphaBeta
    Jogada jogadaDaIA;

    // Construtor vazio
    public Estrutura() { }

    /**
     * Verifica se existe algum movimento valido
     * @return 'true' para alguma posição vazia da matriz 
     */
    public boolean existeJogadaValida() {
        return (vitoriaDaIA() || vitoriaDoUsuario() || getPossiveisJogadas().isEmpty());
    }

    /**
     * Verifica se a IA (X) venceu
     * @return 'true' para vitoria da IA
     */
    public boolean vitoriaDaIA() {
        return verificarVitoria(1);
    }

    /**
     * Verifica se o Usuario (O) venceu
     * @return 'true' para vitoria do Usuario
     */
    public boolean vitoriaDoUsuario() {
        return verificarVitoria(2);
    }

    /**
     * Verifica a vitoria dependendo do jogador
     * @param jogador - '1' para IA ou '2' para Jogador
     * @return 'true' caso obtenha a vitoria 
     */
    public boolean verificarVitoria( int jogador ){
        /*  -   |       |
         *      |   -   |
         *      |       |   -
         */

        /*      |       |   -
         *      |   -   |
         *   -  |       |
         */
        if ((estrutura[0][0] == estrutura[1][1] && estrutura[0][0] == estrutura[2][2] && estrutura[0][0] == jogador) ||
            (estrutura[0][2] == estrutura[1][1] && estrutura[0][2] == estrutura[2][0] && estrutura[0][2] == jogador)) {
            return true;
        }
        /*  -   |   -   |   -
         *      |       |
         *      |       |
         */

        /*      |       |
         *   -  |   -   |   -
         *      |       |
         */

        /*      |       |
         *      |       |
         *   -  |   -   |   -
         */

        /*   -  |       |
         *   -  |       |
         *   -  |       |
         */

        /*      |   -   |
         *      |   -   |
         *      |   -   |
         */

        /*      |       |   -
         *      |       |   -
         *      |       |   -
         */
        for (int i = 0; i < 3; ++i) {
            if (((estrutura[i][0] == estrutura[i][1] && estrutura[i][0] == estrutura[i][2] && estrutura[i][0] == jogador) ||
                (estrutura[0][i] == estrutura[1][i] && estrutura[0][i] == estrutura[2][i] && estrutura[0][i] == jogador))) {

                return true;
            }
        }
        return false;
    }

    /**
     * Retorna todas as posições da matriz não setadas com
     * jogadas da IA (X) ou do Usuario (O)
     * @return Lista de jogadas
     */
    public List<Jogada> getPossiveisJogadas() {
        possiveisJogadas = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                if (estrutura[i][j] == 0) {
                    possiveisJogadas.add(new Jogada(i, j));
                }
            }
        }
        return possiveisJogadas;
    }

    /**
     * Recebe uma jogada e seta nela o simbolo recebido
     * @param jogada - Objeto jogada com a posição jogada
     * @param simbolo - Simbolo '1' para IA ou '2' para Usuario
     */
    public void fazerJogada(Jogada jogada, int simbolo) {
        estrutura[jogada.x][jogada.y] = simbolo;
    }

    /**
     * Encontra na lista de jogadas dos filhos a melhor para 
     * a IA jogar
     * @return Melhor jogada
     */
    public Jogada encontrarMelhorMovimento() {
    	// Incialização da pontuação
        int MAX = -100000;
        // Posição da melhor jogada na lista
        int best = -1;

        for (int i = 0; i < pontuacaoDasFolhas.size(); ++i) {
            if (MAX < pontuacaoDasFolhas.get(i).pontuacao) {
                MAX = pontuacaoDasFolhas.get(i).pontuacao;
                best = i;
            }
        }
        // Retorna a jogada da melhor posição
        return pontuacaoDasFolhas.get(best).jogada;
    }

    /**
     * Converte o valor int para o simbolo a ser mostrado
     * @param x - Posição x da matrix
     * @param y - Posição y da matriz
     * @return ' ' para 0, 'X' para 1 ou 'O' para 2
     */
    public char getValor( int x, int y ){
        if(estrutura[x][y] == 0){
            return ' ';
        }else if(estrutura[x][y] == 1){
            return 'X';
        }else{
            return 'O';
        }
    }

    /**
     * Imprime na tela a estrutura atual do jogo
     */
    public void mostrar() {
        System.out.println();
        System.out.println( "   " + getValor(0,0)  + "   |   " + getValor(0,1) + "   |   " + getValor(0,2) + "   " );
        System.out.println( "-------+-------+-------");
        System.out.println( "   " + getValor(1,0)  + "   |   " + getValor(1,1) + "   |   " + getValor(1,2) + "   " );
        System.out.println( "-------+-------+-------");
        System.out.println( "   " + getValor(2,0)  + "   |   " + getValor(2,1) + "   |   " + getValor(2,2) + "   \n" );
    }

    /**
     * Obtem a menor pontuação para um nó MIN (Usuario)
     * @param list - Lista de pontuações
     * @return O menor valor
     */
    public int getMin(List<Integer> list) {
        int min = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) < min) {
                min = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    /**
     * Obtem a maior pontuação para um nó MAX (IA)
     * @param list - Lista de pontuações
     * @return O maior valor
     */
    public int getMax(List<Integer> list) {
        int max = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) > max) {
                max = list.get(i);
                index = i;
            }
        }
        return list.get(index);
    }

    /**
     * Chama o minimax recursivo
     * @param nivel - Nivel atual da arvore
     * @param vez - '1' vez da IA ou '2' vez do usuario
     */
    public void chamadaMinimax(int nivel, int vez){
        pontuacaoDasFolhas = new ArrayList<>();
        minimax(nivel, vez);
    }

    /**
     * Implementação do minimax
     * @param nivel - Nivel da arvore
     * @param vez - '1' vez da IA, '2' vez do Usuario
     * @return A pontuação do nó
     */
    public int minimax(int nivel, int vez) {
    	// Verifica se vitoria da IA
        if (vitoriaDaIA()) return +1;        
        // Verifica se vitoria do Usuario
        if (vitoriaDoUsuario()) return -1;
        
        // Cria uma lista de todas as jogadas possiveis
        List<Jogada> jogadasDisponiveis = getPossiveisJogadas();

        // Verifica se empate
        if (jogadasDisponiveis.isEmpty()) return 0;
        
        // Cria uma lista de pontuação do nivel atual
        List<Integer> pontuacao = new ArrayList<>();
        // Para cada possivel jogada chama a recursividade
        for (int i = 0; i < jogadasDisponiveis.size(); ++i) {
            Jogada jogada = jogadasDisponiveis.get(i);

            // Vez da IA
            if (vez == 1) {
            	// Faz a jogada
                fazerJogada(jogada, 1);
                // Adiciona na lista de pontuação o resultado da proxima chamda 
                // do minimax aumentando o nivel em +1 e alternando a vez
                int pontuacaoDaUltimaJogada = minimax(nivel + 1, 2);
                pontuacao.add(pontuacaoDaUltimaJogada);

                // Se folha
                if (nivel == 0){
                	// Adiciona na lista de folhas a jogada dessa folha
                    pontuacaoDasFolhas.add(new JogadaAtual(pontuacaoDaUltimaJogada, jogada));

                }
            // Se vez do usuario
            } else if (vez == 2) {
            	// Faz a jogada 
                fazerJogada(jogada, 2);
             // Adiciona na lista de pontuação o resultado da proxima chamda 
                // do minimax aumentando o nivel em +1 e alternando a vez
                pontuacao.add(minimax(nivel + 1, 1));
            }
            // Limpa a ultima jogada da estrutura 
            // Porque não é uma jogada oficial
            estrutura[jogada.x][jogada.y] = 0; 
        }
        // Ao fim da recursividade de cada nó, se vez da IA retorna o maior valor
        // se vez do Usuario retorna o menor valor
        return vez == 1 ? getMax(pontuacao) : getMin(pontuacao);
    }

    /**
     * Implementação do Minimax com a poda alpha beta
     * @param nivel - Nivel da arvore
     * @param vez - '1' vez da IA, '2' vez do Usuario
     * @return A pontuação do nó
     */
    public int minimaxAlphaBeta(int nivel, int vez) {
    	// Verifica se vitoria da IA
        if (vitoriaDaIA()) return +1;
        // Verifica se vitoria do Usuario
        if (vitoriaDoUsuario()) return -1;

        // Cria uma lista de todas as jogadas possiveis
        List<Jogada> jogadasDisponiveis = getPossiveisJogadas();

        // Verifica se empate
        if (jogadasDisponiveis.isEmpty()) return 0;

        // Inicializa a pontuação dos min e max
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        
        // Para cada possivel jogada chama a recursividade
        for (int i = 0; i < jogadasDisponiveis.size(); ++i) {
            Jogada jogada = jogadasDisponiveis.get(i);
         
            // Vez da IA
            if (vez == 1) {
            	// Faz a jogada
                fazerJogada(jogada, 1);
                // Seta a pontuação do nó, chamando o minimax com o proximo nivel e alterando a vez
                int pontuacaoMax = minimax(nivel + 1, 2);
                // Calcula a maior pontuação entre o max atual e o da pontuação desse nó
                max = Math.max(pontuacaoMax, max);
                
                // Verifica se a pontuação atual é um empate e/ou vitoria da IA
                // Se for seta como jogada para IA
                if(pontuacaoMax >= 0){
                    if(nivel == 0)
                        jogadaDaIA = jogada;
                }
                // Se já for uma vitoria, para de procurar vitorias e empates 
                if(pontuacaoMax == 1){
                	// Limpa a ultima jogada porque não é uma jogada oficial
                    estrutura[jogada.x][jogada.y] = 0;
                    break;
                }
                // Se for a ultima jogada possivel e não tiver encotrado 
                // uma jogada boa, faça essa como a jogada da IA
                if(i == jogadasDisponiveis.size()-1 && max < 0){
                    if(nivel == 0) jogadaDaIA = jogada;
                }
            // Vez do Usuario
            } else if (vez == 2) {
            	// Faz a jogada
                fazerJogada(jogada, 2);
             // Seta a pontuação do nó, chamando o minimax com o proximo nivel e alterando a vez
                int pontuacaoMin = minimax(nivel + 1, 1);
             // Calcula a menor pontuação entre o min atual e o da pontuação desse nó
                min = Math.min(pontuacaoMin, min);
                if(min == -1){
                	// Limpa a posição porque não é umajogada oficial
                    estrutura[jogada.x][jogada.y] = 0;
                    // Se já encontrar uma vitoria para o jogador para de procurar
                    break;
                }
            }
            // Limpa a ultima jogada porque não é uma jogada oficial
            estrutura[jogada.x][jogada.y] = 0; 
        }
        // Assim que encontra uma vitoria para a IA retorna esse valor ou se vez 
        // do Usuario retorna sua primeira vitoria encontrada
        return vez == 1 ? max : min;
    }
}
